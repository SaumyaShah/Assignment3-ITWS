#!/bin/bash

read str

flag=0

n=$( echo -n $str | wc -c )

fact=1
#CALCULATING FACTORIAL OF n
for((i=1;i<=n;i++))
do
	fact=$((fact*i))
done

let loop=fact*4

for((i=0;i<loop;i++))
do
	comm=$( echo "$str" | sed 's/./&\n/g' | shuf | tr -d "\n" )
	#DISPOSING OF UNWANTED OUTPUT STREAMS
	which $comm &>/dev/null

	if [ `echo $?` -eq 0 ]
	then
		echo "$comm"
		echo "Arguments ==>"
		echo "Yes"
		flag=1
		
		nline=$( $comm --help | grep '^ ' | wc -l )
		needline=`expr $nline - 2`
		#REMOVING SOME EXTRA LINES FROM OUTPUT
		$comm --help | grep '^ ' | head -$needline
	
		#IF CORRECT COMMAND IS FOUND!
		break
	fi

done

if [ $flag -eq 0 ]
then
	echo "No"
fi
