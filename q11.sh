#!/bin/bash
#FOR MULTIPLICATION ARGUMENT MUST BE '/*'

arr=( "$@" )

n=${#arr[@]}

case $1 in
	
	+)
		sum=0
		
		for((i=2;i<$n;i++))
		do
			sum=$( echo "$sum"+"${arr[$i]}" | bc -l )
		done
		
		printf '%.4f\n' "$sum"
	
		;;
	
	-)
		diff=${arr[2]}
	
       		for((i=3;i<$n;i++))
                do
			 diff=$( echo "$diff"-"${arr[$i]}" | bc -l )
                done
		
		printf '%.4f\n' "$diff"
		
		;;

	\*)
		prod=1
		
		for((i=2;i<$n;i++))
                do
			prod=$( echo "$prod"*"${arr[$i]}" | bc -l )
                done 
		
		printf '%.4f\n' "$prod"
                
		;;
	
	/) div=${arr[2]}
                
		for((i=3;i<$n;i++))
                do
			div=$( echo "scale=5 ; $div / ${arr[$i]}" | bc -l )
                done
		
		printf '%.4f\n' "$div"
                       
	       	;;
esac
