#INPUT FORMAT : Jan 21 2016

#!/bin/bash

flag=1

case "$1" in 
	
	Jan)
		m=01
	
		if [ $2 -gt 31 ]
		then
			echo "invalid"
			flag=0
		fi
	
		;;
	
	Feb)
		
		y=$(($3%4))

		if [ $2 -gt 28 ] && [ $y -ne 0 ]
		then
			echo "invalid" 
			flag=0
		fi
		
		if [ $2 -gt 29 ] && [ $y -eq 0 ]
		then
			echo "invalid"
			flag=0
		fi
		
		m=02
		;;
	
	Mar)
		m=03
	
		if [ $2 -gt 31 ]
		then
			echo "invalid"
			flag=0
		fi
	
		;;
	
	Apr)
		m=04

		if [ $2 -gt 30 ]
		then
			echo "invalid"
			flag=0
		fi
	
		;;
	
	May)
		m=05
	
		if [ $2 -gt 31 ]
		then 
			echo "invalid"
			flag=0
		fi
	
		;;
	
	Jun)
		m=06
	
		if [ $2 -gt 30 ]
		then 
			echo "invalid"
			flag=0
		fi
	
		;;
	
	Jul)
		m=07
	
		if [ $2 -gt 31 ]
		then
			echo "invalid"
			flag=0
		fi
	
		;;
	
	Aug)
		m=08

		if [ $2 -gt 31 ]
		then
			echo "invalid"
			flag=0
		fi
		
		;;

	Sep)
		m=09

		if [ $2 -gt 30]
	        then
			echo "invalid"
			flag=0
		fi	

		;;

	Oct)
		m=10

		if [ $2 -gt 31 ]
	        then 	
			echo "invalid"
			flag=0
		fi

		;;

	Nov)
		m=11

		if [ $2 -gt 30 ]
		then 
			echo "invalid"
			flag=0
		fi

		;;

	Dec)
		m=12

		if [ $2 -gt 31 ]
		then 
			echo "invalid"
			flag=0
		fi

		;;

esac

if [ $flag -eq 1 ]
then 
	echo "$m/$2/$3"
	echo "$2-$m-$3"
fi
