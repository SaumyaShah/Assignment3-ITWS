#!/bin/bash

#TO CHECK WHETHER CRONTAB IS VALID OR NOT , "&>/dev/null/" TO DISPOSE UNWANTED OUTPUT
crontab $1 &>/dev/null  

flag=$( echo $? )

if [ $flag -eq 0 ]
then 
	echo "Yes"
else
	echo "No"
fi
