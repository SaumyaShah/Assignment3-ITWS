#!/bin/bash

num=("$@")

n=$#

ans=${num[0]}

for((i=1;i<$n;i++))
do
	ans=$( echo "$ans^${num[$i]}" |bc -l ) 
done

echo $ans
