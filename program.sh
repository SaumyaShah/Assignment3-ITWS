#!/bin/bash
case "$1" in 
	read)
		case "$2" in
			eno)
				#TO SEARCH THROUGH EMPLOYEE'S NUMBER ($3) IN COLUMN AFTER PASSING 0 "," i.e. FIRST COLUMN
				grep -E  "^([^,]*\,){0}$3" employee.txt
				;;
		esac
	
		case "$2" in
			ename)
				 #TO SEARCH THROUGH  EMPLOYEE'S NAME ($3) IN COLUMN AFTER PASSING 1 "," i.e. SECOND COLUMN
				grep -E  "^([^,]*\,){1}$3" employee.txt
				;;
		esac
	
       		case "$2" in
                        salary)
				 #TO SEARCH THROUGH  EMPLOYEE'S SALARY ($3) IN COLUMN AFTER PASSING 2 "," i.e. THIRD COLUMN
				grep -E  "^([^,]*\,){2}$3" employee.txt
				;;
		esac
		;;
	esac

	
case "$1" in 
	write)
		echo "$2,$3,$4" | cat >> employee.txt
		echo "Done"
		;;
esac


case "$1" in
	update) 
		#TO SEARCH THROUGH EMPLOYEE'S NUMBER ($2) IN COLUMN AFTER PASSING 0 "," i.e. FIRST COLUMN AND STORE WHOLE LINE IN "rep"
		rep=$( grep -E  "^([^,]*\,){0}$2" employee.txt )
		
		#FIND LINE STARTING WITH "rep" AND REPLACE IT BY GIVEN INPUT
		echo "$rep" |  xargs -I{} sed -i -e " s/{}/$2,$3,$4/" employee.txt
	
		echo "Done"
		;;
esac


case "$1" in 
	delete)
		#FIND LINE STARTING WITH GIVEN EMPLOYEE NUMBER AND DELETE IT
		sed -i "/^$2/ d" employee.txt
		echo "Done"
		;;
esac


case "$1" in
	duplicate)
		
		numline=$( cat employee.txt | wc -l )

		#WE NEED NOT TO CONSIDER LINE WITH TITLE.
		let needline=numline-1
		
		#READING ALL LINE AND COUNTING FREQUENCY OF LINE
		i=1				
		cat employee.txt | tail -$needline |sort | uniq -c | tr -s ' ' | while read line
		do
			count=$( echo $line | cut -d " " -f 1 | head -$i | tail -1 )
			dup=$( echo $line | cut -d " " -f 2 | head -$i | tail -1 )
			
			#FINDING DUPLICATE LINES
			if [ $count -gt 1 ]
			then
			grep "$dup" employee.txt | sort | uniq
			fi
			
			let i++
		done
	
		;;
esac

case "$1" in
	nthsalary)

	line=$( cat employee.txt | wc -l )

#WE NEED NOT TO CONSIDER LINE WITH TITLES.
	let needline=line-1

#SEARCHING FOR THE VALUE OF nth HIGHEST SALARY
	salary=$( cat employee.txt | cut -d "," -f 3 | tail -$needline | sort -nr | uniq | head -$2 | tail -1 )

#SEARCHING FOR EMPLOYEES EARNING THAT SALARY
	grep -E  "^([^,]*\,){2}$salary" employee.txt
esac

