#!/bin/bash

#FINDING CURRENT DIRECTORY
cd ~
currdir=$( echo $PWD | rev | cut -d '/' -f 1 | rev )

echo "<!DOCTYPE html>" > q14.html 
echo "<html>"  >> q14.html
	echo "<head>" >> q14.html
		echo "<title>HOME DIRECTORY</title>" >> q14.html
	echo "</head>" >> q14.html

	echo "<style>" >> q14.html
		echo "table,td,th,tr { border: 1px solid black;  border-collapse: collapse;}" >> q14.html
	echo "</style>" >> q14.html

	echo "<body>" >> q14.html
		echo "<table style="width:100%"> " >> q14.html

#COMMAND TO INCLUDE HIDDEN FILES: 'ls -laRh'
ls -lRh | while read line 
do
	char=$(  echo $line | cut -c 1 )

	if [[ "$char" == "." ]]
	then
		
		#REPLACING ./ WITH CURRENT DIRECTORY NAME
		fline=$(  echo $line | sed -e "s/\./$currdir/g" )

       		echo "<tr><th colspan="3">$fline</th></tr>"  >> q14.html
		echo "<tr>"  >> q14.html
			echo "<th>Name</th>"  >> q14.html
			echo "<th>Size</th>" >> q14.html
			echo "<th>File/Dir</th>" >> q14.html
		echo "</tr>" >> q14.html

	elif [[ "$char" == "t" ]]
	then
		#E IS total size OF DIRECTORY. AS SIZE IS IN HUMAN READABLE FORM , IT IS NECESSARY TO CONVERT e TO INTEGER
		e=$( echo $line | tr -s ' ' | cut -d" " -f 2 | cut -d"." -f 1 | cut -d"K" -f 1 | cut -d"M" -f 1 | cut -d"G" -f 1 )
	
		if [ $e -eq 0 ]
		then 
			echo "<tr>"  >> q14.html
           	   	  	echo "<td>EMPTY DIRECTORY/CONTAINS ALL EMPTY FILES</td>"  >> q14.html
                		echo "<td>----</td>" >> q14.html
                		echo "<td>----</td>" >> q14.html
                	echo "</tr>" >> q14.html
		fi

	elif [[ "$char" == "-" ]]
	then 

		size=$( echo  $line | tr -s ' ' | cut -d" " -f 5 )
		file=$( echo  $line | tr -s ' ' | cut -d" " -f 9 )
	
		echo "<tr>"  >> q14.html
	 		echo "<td>$file</td>"  >> q14.html
			echo "<td>$size</td>" >> q14.html
               		echo "<td>File</td>" >> q14.html
		echo "</tr>" >> q14.html

	elif [[ "$char" == "d" ]]
	then
		size=$( echo  $line | tr -s ' ' | cut -d" " -f 5 )
		file=$( echo  $line | tr -s ' ' | cut -d" " -f 9 )

		echo "<tr>"  >> q14.html
			echo "<td>$file</td>"  >> q14.html
			echo "<td>$size</td>" >> q14.html
			echo "<td>Dir</td>" >> q14.html
		echo "</tr>" >> q14.html
	 fi

done
		echo "</table>" >> q14.html
	echo "</body>" >> q14.html
echo "</html>"  >> q14.html

firefox q14.html

