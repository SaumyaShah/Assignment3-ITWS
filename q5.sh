#!/bin/bash

array=( "$@" )
n=${#array[@]}

for((i=2;i<$n;i++))
do
	m=$( echo "$i - 2")

	#REPLACING EACH ARRARY ELEMENT BY -k<element>
	ar[$m]=$( echo -e " ""${array[$i]}" "\c" | sed s/"${array[$i]}"/"-k${array[$i]}"/g )
done

cat $1 $2 | sort -g ${ar[@]}

