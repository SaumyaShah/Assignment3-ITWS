#!/bin/bash

#Bash disables history in noninteractive shells by default
HISTFILE=~/.bash_history
set -o history
 
other=$( cat ~/.bash_history | cut -d" " -f 1 | sort | uniq -c | sort -nr )

p=$( history | tr -s ' '  | grep -c  "|" )
pipe=$( echo "$p PIPE" )

echo -e "$other" "\n" "$pipe" | sort -nr | tr -s ' '

