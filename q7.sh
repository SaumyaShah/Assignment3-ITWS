#!/bin/bash

read string

read -a ARRAY <<< $(echo $string | sed 's/./& /g')


i=0
flag=1

count=${#ARRAY[@]}

#ARRAY OF UPPERCASE CHARACTERS
newstring=$( echo $string | tr '[:lower:]' '[:upper:]' )

for (( i=0 ; i < ${#newstring} ; i++ ))
do
    ARR[$i]=${newstring:i:1}
done

for((i=0;i<$count;i++))
do
        j=`expr n-i-1`

    #CHECKING FOR BOTH ARRAYS ( UPPERCASE AND LOWERCASE )
	if [ ${ARRAY[$i]} != ${ARRAY[$j]} ] &&  [ ${ARR[$i]} != ${ARR[$j]} ]
        then
                flag=0
                break
        fi

done

if [ $flag -eq 0 ]
then
        echo "No"
else
        echo "Yes"
fi

